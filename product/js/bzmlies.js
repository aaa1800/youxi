
$(document).ready(function() {
	//头部
    $(".top_box li:not(.not)").live({
        mouseenter: function () {
            $(this).addClass("curr")
        },
        mouseleave: function () {
            $(this).removeClass("curr")
        }
    });
    
 $(".sort_page a:not([data-ajax])").click(function() {
        var a = $("#page .ohave"),
            b = $(this).attr("id");
        a = ("l" == b ? a.prev() : a.next()).find("a");
        if (0 >= a.length) return layer.alert("亲，已经是" + ("l" == b ? "第一" : "最后一") + "页啦！", {
            icon: 5
        }), !1;
        location.href = a.attr("href")
    });
    $(".sort_page a[data-ajax]").click(function() {
        var a = parseInt($(this).siblings("#curr-page").html()),
            b = parseInt($(this).siblings("#total-page").html());
        a = "l" == $(this).attr("id") ? a - 1 : a + 1;
        if (a > b || 0 >= a) return layer.alert("亲，已经是" + (a < b ? "第一" : "最后一") + "页啦！", {
            icon: 5
        });
        fixed_tool(a)
    });
    $(".sort_select dl").hover(function() {
        var a = $(this).attr("data");
        a = $(this).find("a[data='" + a + "']");
        (0 >= a.length ? $(this).find("span:first") : a).addClass("curr");
        $(this).addClass("curr").siblings("dl").removeClass("curr");
        $(this).find("dd").show()
    }, function() {
        $(this).removeClass("curr").find("dd").hide()
    });
    $(".lrtop cite  a").click(function() {
        $(this).addClass("curr").siblings().removeClass("curr").closest("dl").next(".post").find("div:eq(" + $(this).index() + ")").removeClass("hide").siblings().addClass("hide")
    });
    
	   $(".shou").hover(function () {
		    var that = $(this);
		    nowtips = layer.tips('商家手动发货商品，付款后联系商家发货。', that, {
		        tips: [1, '#999'],
		        time: 10000
		    });
		}, function () {
		    layer.close(nowtips);
		});
		
		$(".zhe").hover(function () {
		    var that = $(this);
		    nowtips = layer.tips('该商品已经加入会员等级折扣体系，本站会员可以享受相应的折价优惠', that, {
		        tips: [1, '#40affe'],
		        time: 10000
		    });
		}, function () {
		    layer.close(nowtips);
		});
		
		$(".yan").hover(function () {
		    var that = $(this);
		    nowtips = layer.tips('交易成功后，货款延迟1天转到卖家账户。', that, {
		        tips: [1, '#ff7e00'],
		        time: 10000
		    });
		}, function () {
		    layer.close(nowtips);
		});
		
		$(".tuiguang").hover(function () {
		    var that = $(this);
		    nowtips = layer.tips('商品已加入推广服务,在商品列表置顶显示', that, {
		        tips: [1, '#ffc107'],
		        time: 10000
		    });
		}, function () {
		    layer.close(nowtips);
		});
		
		
		$(".zidong").hover(function () {
		    var that = $(this);
		    nowtips = layer.tips('平台代发货商品，拍下后，即可收到平台自动发出的商品', that, {
		        tips: [1, '#cc805b'],
		        time: 10000
		    });
		}, function () {
		    layer.close(nowtips);
		});
		
		$(".mysj").hover(function () {
		    var that = $(this);
		    var my = $(that).data("my");
		    nowtips = layer.tips('' + my + '', that, {
		        tips: [1, '#fff'],
		        time: 10000
		    });
		}, function () {
		    layer.close(nowtips);
		});
		
		$(".bao-t").hover(function () {
		    var that = $(this);
		    var mn = $(that).data("mn");
		    nowtips = layer.tips('已加入消保，商家已缴纳保证金 <span class="mn-p">￥：' + mn + '元</span> ', that, {
		        tips: [1, '#0f9c0b'],
		        time: 10000
		    });
		}, function () {
		    layer.close(nowtips);
		});
		
    $(".clist dt,.wlist .pic").mouseenter(function() {
        var a = $(this);
        enter = setTimeout(function() {
            a.find(".ly").show()
        }, 300)
    }).mouseleave(function() {
        clearTimeout(enter);
        $(this).find(".ly").hide()
    });
	
});




