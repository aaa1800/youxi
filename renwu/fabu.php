<?
include("../config/conn.php");
include("../config/function.php");

$ty1id=intval($_GET[ty1id]);$ty1name=returntasktype(1,$ty1id);
$ty2id=intval($_GET[ty2id]);$ty2name=returntasktype(2,$ty2id);
$_SESSION["tzURL"]=weburl."renwu/fabu.php?ty1id=".$ty1id."&ty2id=".$_GET[ty2id];
sesCheck();

if($_GET[control]=="add"){
 zwzr();
 $wczq=intval($_POST[twczq]);
 $yusuan1=intval($_POST[tyusuan1]);
 $yusuan2=intval($_POST[tyusuan2]);
 if($yusuan1==$yusuan2){$yusuantype=1;}else{$yusuantype=2;}
 $xyrs=intval($_POST[txyrs]);
 if($wczq<=0 || $yusuan1<=0 || $yusuan2<=0 || $xyrs<=0){Audit_alert("参数有误","fabu.php");}
 $bh=returnbh()."-".$rowuser[id];
 if(empty($rowcontrol[taskok])){$zt=1;}else{$zt=0;}
 intotable("yjcode_renwu","bh,ty1id,ty2id,userid,sj,lastsj,wczq,bmjzsj,tit,txt,yusuan1,yusuan2,yusuantype,xyrs,djl,zt,renwuzt,ingnum","'".$bh."',".intval($_POST[ty1id]).",".intval($_POST[ty2id]).",".$rowuser[id].",".strtotime($sj).",".strtotime($sj).",".$wczq.",".strtotime(sqlzhuru($_POST[tbmjzsj])).",'".sqlzhuru($_POST[ttit])."','".sqlzhuru1($_POST[content])."',".$yusuan1.",".$yusuan2.",".$yusuantype.",".$xyrs.",1,".$zt.",1,0");
 $id=mysqli_insert_id($conn);
 php_toheader("view".$id.".html");
 
}
?>
<html>
<head>
<meta http-equiv="x-ua-compatible" content="ie=7" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>发起任务 - <?=webname?></title>
<? include("../tem/cssjs.html");?>
<script language="javascript" src="../js/adddate.js"></script>
<script type="text/javascript" charset="utf-8" src="../config/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="../config/ueditor/ueditor.all.min.js"> </script>
<script type="text/javascript" charset="utf-8" src="../config/ueditor/lang/zh-cn/zh-cn.js"></script>
</head>
<body>
<? include("../tem/top.html");?>
<? include("../tem/top1.html");?>
<div class="yjcode">

<div class="dqwz"><ul class="u1"><li class="l1">当前位置：<a href="../">首页</a> > <a href="./">任务大厅</a> > 发布任务</li></ul></div>

<!--左B-->
<div class="fabuleft">
 
 <form name="f1" method="post" onSubmit="return fbtj()">
 <input type="hidden" name="ty1id" value="<?=$ty1id?>" />
 <input type="hidden" name="ty2id" value="<?=$ty2id?>" />
 <div class="fabumain">
  <ul class="u1">
  <li class="l1"><span>*</span> 选择分类</li>
  <li class="l2" onmouseleave="typehtmlout()">
   <input type="text" style="width:480px;cursor:pointer;"<? if(!empty($ty1name)){?> value="<?=$ty1name?> > <?=$ty2name?>"<? }?> onClick="rwtypeonc()" readonly placeholder="请点击选择需求分类" class="inp" name="rwtype" >
   <div class="typehtml" id="typehtml" style="display:none;"></div>
  </li>
  <li class="l1"><span>*</span> 任务标题</li>
  <li class="l2"><input type="text" style="width:480px;" placeholder="请简述需求内容" class="inp" name="ttit" ></li>
  <li class="l3"><span>*</span> 任务详情</li>
  <li class="l4"><script id="editor" name="content" type="text/plain" style="width:100%;height:300px;"></script></li>
  </ul>
 </div>
 
 <div class="fabumain">
  <ul class="u1">
  <li class="l1"><span>*</span> 完成天数</li>
  <li class="l2"><input type="text" style="width:200px;" class="inp" value="15" name="twczq" ><span class="fd">天</span></li>
  <li class="l1"><span>*</span> 报名截止</li>
  <li class="l2">
   <input type="text" onClick="SelectDate(this,'yyyy-MM-dd hh:mm:ss')" style="width:200px;cursor:pointer;" class="inp" readonly name="tbmjzsj" >
   <span class="fd">
   快捷设置：
   <a href="javascript:void(0);" onClick="bmjzkj('<?=date("Y-m-d H:i:s",strtotime("+3 day"));?>')">3天</a> /
   <a href="javascript:void(0);" onClick="bmjzkj('<?=date("Y-m-d H:i:s",strtotime("+7 day"));?>')">7天</a> /
   <a href="javascript:void(0);" onClick="bmjzkj('<?=date("Y-m-d H:i:s",strtotime("+10 day"));?>')">10天</a> /
   <a href="javascript:void(0);" onClick="bmjzkj('<?=date("Y-m-d H:i:s",strtotime("+30 day"));?>')">30天</a> /
   <a href="javascript:void(0);" onClick="bmjzkj('<?=date("Y-m-d H:i:s",strtotime("+365 day"));?>')">365天</a>
   </span>
  </li>
  <li class="l1"><span>*</span> 任务预算</li>
  <li class="l2">
   <input type="text" style="width:200px;" class="inp" placeholder="请输入最低预算" name="tyusuan1" ><span class="fd" style="margin-right:10px;">元 到 </span>
   <input type="text" style="width:200px;" class="inp" placeholder="请输入最高预算" name="tyusuan2" ><span class="fd">元(如果两者相同，表示一口价)</span>
  </li>
  <li class="l1"><span>*</span> 需要人数</li>
  <li class="l2"><input type="text" style="width:200px;" class="inp" value="1" name="txyrs" ><span class="fd">人</span></li>
  <li class="l99"><input type="submit" value="发布任务" /></li>
  </ul>
 </div>
 </form>
 
</div>
<!--左E-->

<!--发布右B-->
<div class="faburight">

 <? include("gloright.php");?>

</div>
<!--发布右E-->

</div>

<script type="text/javascript">
$.post("type.php",{},function(result){
 $("#typehtml").html(result);
});
var ue= UE.getEditor('editor',{toolbars:[['fullscreen', 'source', '|', 'undo', 'redo', '|','removeformat', 'formatmatch' ,'|', 'forecolor','fontsize', '|','link', 'unlink','insertimage', 'emotion', 'attachment']]});
</script>

<div class="clear clear20"></div>
<? include("../tem/bottom.html");?>
</body>
</html>