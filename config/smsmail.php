<?
function smssend($admin,$mot,$smsv,$ifgly){ //$ifgly 0表示会员发，1表示管理员发
 global $conn;
 global $rowcontrol;
 global $fauserid;    //发送方ID
 global $jieuserid;   //接收方ID
 if($rowcontrol["ifmob"]=="on" && preg_match("/^1[345789]\d{9}$/",$mot)){
 //开始短信功能B

 if(!empty($rowcontrol[smsbig]) && empty($ifgly)){
  $sj1=strtotime(date("Y-m-d H:i:s",strtotime("-1 day")));
  $sj2=strtotime(getsj());
  if(returncount("yjcode_smsmaillog where uip='".getuip()."' and admin=2 and sj>".$sj1." and sj<".$sj2."")>=$rowcontrol[smsbig]){echo "errbig";exit;}
 }

 if($admin==1 || $admin==2 || $admin==3 || $admin==4 || $admin==5){
  $sbh="000";
  $str="验证码：".$smsv.",如果不是本人操作，请忽略此信息。";
  $sms_txt="{yzm:'".$smsv."'}";
  if($admin==1){$adminstr="直接登录验证";}
  elseif($admin==2){$adminstr="找回密码";}
  elseif($admin==3){$adminstr="手机解除绑定";}
  elseif($admin==4){$adminstr="手机绑定";}
  elseif($admin==5){$adminstr="注册验证";}
 
 }elseif($admin==6){
  $sbh="004";
  $str="亲，有新订单啦！请尽快登录网站发货，购买商品价格为：".$smsv."元";
  $sms_txt="{tit:'".$smsv."'}";
  $adminstr="订单发货提醒";
 
 }elseif($admin==7){
  $sbh="005";
  $smsvarr=preg_split("/yjcode/",$smsv);
  $str="退款通知：有买家进行了退款，商品单价".$smsvarr[0]."元，数量".$smsvarr[1]."，请尽快登录网站处理";
  $sms_txt="{money1:'".$smsvarr[0]."',num:'".$smsvarr[1]."'}";
  $adminstr="退款处理提醒";
 
 }elseif($admin==8){
  $sbh="007";
  $str="您的工单状态已经变更为：".$smsv;
  $sms_txt="{zttz:'".$smsv."'}";
  $adminstr="工单处理";
 
 }elseif($admin==9){
  $sbh="008";
  $str="您的店铺审核结果为：".$smsv;
  $sms_txt="{shopzt:'".$smsv."'}";
  $adminstr="店铺审核结果通知";
 
 }elseif($admin==10){
  $sbh="009";
  $smsvarr=preg_split("/yjcode/",$smsv);
  $str="ID为".$smsvarr[0]."的商品审核结果为：".$smsvarr[1];
  $sms_txt="{pid:'".$smsvarr[0]."',prozt:'".$smsvarr[1]."'}";
  $adminstr="商品审核结果通知";
 
 }elseif($admin==11){
  $sbh="010";
  $smsvarr=preg_split("/yjcode/",$smsv);
  $str="您的".$smsvarr[0]."元提现申请受理结果为：".$smsvarr[1];
  $sms_txt="{txzt:'".$smsvarr[0]."',money1:'".$smsvarr[1]."'}";
  $adminstr="提现申请受理通知";
 
 }elseif($admin==12){
  $sbh="011";
  $str="您的实名认证审核结果为：".$smsv;
  $sms_txt="{smrz:'".$smsv."'}";
  $adminstr="实名认证通知";
 
 }
 
 if(empty($rowcontrol[smsmode])){ //非阿里通信B
  include(dirname(__FILE__)."/mobphp/mysendsms.php");
  yjsendsms($mot,$str);
 }else{//阿里通信B
  $sqlsms="select * from yjcode_smsmb where mybh='".$sbh."'";mysqli_set_charset($conn,"utf8");$ressms=mysqli_query($conn,$sqlsms);
  if($rowsms=mysqli_fetch_array($ressms)){$sms_mot=$mot;$sms_id=$rowsms[mbid];if(!empty($sms_id)){include(dirname(__FILE__)."/mobphp/mysendsms.php");}}
 }
 
 updatetable("yjcode_control","smskc=smskc-1");
 intotable("yjcode_smsmaillog","admin,fa,fauserid,userid,txt,sj,uip","2,'".$mot."',".intval($fauserid).",".intval($jieuserid).",'".$adminstr."',".returnsjc().",'".getuip()."'");
 
 //开始短信功能E
 }
}

function emailsend($admin,$m,$mailvar){
 require(dirname(__FILE__)."/mailphp/sendmail.php");
 if($admin==1){ //验证码
  $txt="验证码：<font color='red' style='font-size:18px;'>".$mailvar."</font><br>如果不是本人操作，请忽略此信息。【".webname."】<hr>该邮件为系统发出，请勿回复";
  $tit="邮箱绑定【".webname."】";
 
 }elseif($admin==2){ //找回密码
  $txt="验证码：<font color='red' style='font-size:18px;'>".$mailvar."</font><br>如果不是本人操作，请忽略此信息。【".webname."】<hr>该邮件为系统发出，请勿回复";
  $tit="找回密码【".webname."】";

 }elseif($admin==3){ //订单通知
  $txt=$mailvar;
  $tit="订单提醒【".webname."】";
 
 }elseif($admin==4){ //工单回复
  $a=preg_split("/yjcode/",$mailvar);
  $lj=weburl."user/gdlist.php";
  $tit="您好，您的工单".strip_tags($a[0]);
  $txt="尊敬的用户：".$a[1]." 您好：<br>";
  $txt=$txt."感谢您对".webname."(".weburl.")的支持!<br>";
  $txt=$txt."您于".$a[2]."提交工单状态已经变更为：".$a[0]."。您可以访问以下链接登录网站查看工单详情<br><a href='".$lj."' target='_blank'>".$lj."</a><hr>";
  $txt=$txt."本邮件由系统自动发出，请勿直接回复！";
 
 }elseif($admin==5){ //任务接手提醒
  $a=preg_split("/yjcode/",$mailvar);
  $tit="任务接手提醒【".webname."】";
  $txt="有人给出报价了，请尽快处理。<br>任务：".$a[0]."<br>报价：<font color='red' style='font-size:18px;'>".$a[1]."</font><br>【".webname."】<hr>该邮件为系统发出，请勿回复";
 
 }elseif($admin==6){ //店铺审核结果
  $tit="店铺审核结果(".$mailvar.")【".webname."】";
  $txt="您在本站的店铺审核结果为：<font color='red' style='font-size:18px;'>".$mailvar."</font><br>【".webname." ".weburl."】<hr>该邮件为系统发出，请勿回复";
 
 }elseif($admin==7){ //商品审核结果
  $a=preg_split("/yjcode/",$mailvar);
  $lj=weburl."product/view".$a[2].".html";
  $tit="审核结果(".$a[0].")【".webname."】";
  $txt="您发布的商品：".$a[1]."<br>审核结果：<font color='red' style='font-size:18px;'>".$a[0]."</font><br>商品链接：<a href='".$lj."' target='_blank'>".$lj."</a><hr>【".webname."】<br>该邮件为系统发出，请勿回复";
 
 }elseif($admin==8){ //商品提现结果
  $a=preg_split("/yjcode/",$mailvar);
  $tit="提现处理结果为(".$a[0].")【".webname."】";
  $jg=$a[0];
  if(!empty($a[2])){$jg=$jg." (".$a[2].")";}
  $txt="您申请的：".$a[1]."元提现受理结果为：<font color='red' style='font-size:18px;'>".$jg."</font><hr>【".webname."】<br>该邮件为系统发出，请勿回复";
 
 }elseif($admin==9){ //实名认证审核结果
  $tit="实名认证结果为(".$mailvar.")【".webname."】";
  $txt="您的实名认证结果为：<font color='red' style='font-size:18px;'>".$mailvar."</font><hr>【".webname."】<br>该邮件为系统发出，请勿回复";

 }
 yjsendmail($tit,$m,$txt);

}

?>