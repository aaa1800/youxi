<?
include("../../config/conn.php");
include("../../config/function.php");
sesCheck_m();
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="x-ua-compatible" content="ie=7" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
<title>会员中心 <?=webname?></title>
<? include("../tem/cssjs.html");?>
<link href="css/sell.css?t=<?=$glosxbh?>" rel="stylesheet" type="text/css" />
</head>
<body>
<? 
include("topuser.php");
$glotopfix=1;
$glotopback="sell.php";
$glotoptit="商家待办事项";
include("../tem/glotop.php");
?>

<? 
$needfh=returncount("yjcode_order where selluserid=".$rowuser[id]." and ddzt='wait' and admin=1"); //需要发货
$needtk=returncount("yjcode_order where selluserid=".$rowuser[id]." and ddzt='back' and admin=1"); //需要处理退款
$needhf=returncount("yjcode_order where ifpj=1 and selluserid=".$rowuser[id]." and (hftxt='' or hftxt is null) and admin=2"); //评价回复
$needfw=returncount("yjcode_serverorder where selluserid=".$rowuser[id]." and ddzt=1"); //服务确认
$serverneedtk=returncount("yjcode_serverorder where selluserid=".$rowuser[id]." and ddzt=7"); //服务订单退款
$serverneedjf=returncount("yjcode_serverorder where selluserid=".$rowuser[id]." and ddzt=10"); //服务订单纠纷处理
$orderjf=returncount("yjcode_order where selluserid=".$rowuser[id]." and ddzt='jf' and admin=1"); //商品交易纠纷中
$prowdhf=returncount("yjcode_wenda where selluserid=".$rowuser[id]." and hftxt=''"); //商品问答回复
?>

<div class="daiban box">
<div class="dmain flex">

 <? if($needfh>0){?>
 <div class="d1"><a href="sellorder.php?ddzt=wait"><?=$needfh?></a><span>需要发货商品</span></div>
 <? }?>

 <? if($needtk>0){?>
 <div class="d1"><a href="sellorder.php?ddzt=back"><?=$needtk?></a><span>商品处理退款</span></div>
 <? }?>

 <? if($needhf>0){?>
 <div class="d1"><a href="propjlist.php?ifhf=no"><?=$needhf?></a><span>商品评价回复</span></div>
 <? }?>

 <? if($needfw>0 && $rowcontrol[serverhave]==1){?>
 <div class="d1"><a href="sellserverorder.php?ddzt=1"><?=$needfw?></a><span>服务订单确认</span></div>
 <? }?>

 <? if($serverneedtk>0 && $rowcontrol[serverhave]==1){?>
 <div class="d1"><a href="sellserverorder.php?ddzt=7"><?=$serverneedtk?></a><span>服务订单退款</span></div>
 <? }?>

 <? if($serverneedjf>0 && $rowcontrol[serverhave]==1){?>
 <div class="d1"><a href="sellserverorder.php?ddzt=10"><?=$serverneedjf?></a><span>服务订单纠纷处理</span></div>
 <? }?>

 <? if($prowdhf>0){?>
 <div class="d1"><a href="prowdlist.php?ifhf=no"><?=$prowdhf?></a><span>商品问答回复</span></div>
 <? }?>

 <? if($orderjf>0){?>
 <div class="d1"><a href="sellorder.php?ddzt=jf"><?=$orderjf?></a><span>商品交易纠纷</span></div>
 <? }?>
 
</div>
</div>

<? include("../tem/globottom.php");?>

</body>
</html>