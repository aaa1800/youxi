<?
include("../../config/conn.php");
include("../../config/function.php");
sesCheck_m();
?>
<html>
<head>
<meta http-equiv="x-ua-compatible" content="ie=7" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
<title>会员中心 <?=webname?></title>
<? include("../tem/cssjs.html");?>
<link href="css/sell.css?t=<?=$glosxbh?>" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../swiper/css/swiper.min.css">
<script src="../swiper/js/swiper.min.js"></script>
<style type="text/css">
body{background:url(img/sellbg.png) center top no-repeat;background-size:100% 200px;background-color:#f4f4f4;}
</style>
</head>
<body>
<? 
include("topuser.php");
$needfh=returncount("yjcode_order where selluserid=".$rowuser[id]." and ddzt='wait' and admin=1"); //需要发货
$needtk=returncount("yjcode_order where selluserid=".$rowuser[id]." and ddzt='back' and admin=1"); //需要处理退款
$orderjf=returncount("yjcode_order where selluserid=".$rowuser[id]." and ddzt='jf' and admin=1"); //商品交易纠纷中
$needhf=returncount("yjcode_order where ifpj=1 and selluserid=".$rowuser[id]." and (hftxt='' or hftxt is null) and admin=2"); //评价回复
if($rowcontrol[serverhave]==1){
$needfw=returncount("yjcode_serverorder where selluserid=".$rowuser[id]." and ddzt=1"); //服务确认
$serverneedtk=returncount("yjcode_serverorder where selluserid=".$rowuser[id]." and ddzt=7"); //服务订单退款
$serverneedjf=returncount("yjcode_serverorder where selluserid=".$rowuser[id]." and ddzt=10"); //服务订单纠纷处理
}
$prowdhf=returncount("yjcode_wenda where selluserid=".$rowuser[id]." and hftxt=''"); //商品问答回复
$tznum=$needfh+$needtk+$needhf+intval($needfw)+intval($serverneedtk)+intval($serverneedjf)+$prowdhf+$orderjf;
?>

<div class="selltop box">
 <div class="d1"><a href="un.php"><img src="img/selltop1.png" /></a></div>
 <div class="d2 flex">
  <span class="s1" onClick="gourl('selldaiban.php')"><?=$tznum?></span>
  <img src="img/selltop3.png" class="img2" />
  <a href="shezhi.php"><img src="img/selltop2.png" class="img1" /></a>
 </div>
</div>

<div class="selltop1 box">
 <div class="d1"><img src="../../upload/<?=$rowuser[id]?>/shop.jpg" onerror="this.src='../../img/none60x60.gif'" /></div>
 <div class="d2 flex">
  <span class="s1"><?=$rowuser[shopname]?></span>
  <span class="s2" onClick="gourl('openshop4.php')">店铺到期 <?=$rowuser[dqsj]?></span>
 </div>
</div>

<div class="selltop2 box">
 <div class="d1 flex">
  <span class="s1">发货商品</span>
  <a class="s2" href="sellorder.php?ddzt=wait"><?=$needfh?></a>
 </div>
 <div class="d1 flex">
  <span class="s1">退款商品</span>
  <a class="s2" href="sellorder.php?ddzt=back"><?=$needtk?></a>
 </div>
 <div class="d1 flex">
  <span class="s1">评价回复</span>
  <a class="s2" href="propjlist.php?ifhf=no"><?=$needhf?></a>
 </div>
 <? if($rowcontrol[serverhave]==1){?>
 <div class="d1 flex">
  <span class="s1">服务确认</span>
  <a class="s2" href="sellserverorder.php?ddzt=1"><?=$needfw?></a>
 </div>
 <? }?>
</div>

<div class="selltop3 box">
 <div class="dmain flex">
  <div class="d1"><a href="pay.php"><img src="img/sell1.png" ><br>充值</a></div>
  <div class="d1"><a href="paylog.php"><img src="img/sell2.png" ><br>收支明细</a></div>
  <div class="d1"><a href="jflog.php"><img src="img/sell3.png" ><br>积分</a></div>
  <div class="d1"><a href="jfbank.php"><img src="img/sell4.png" ><br>积分银行</a></div>
  <div class="d1"><a href="tixian.php"><img src="img/sell5.png" ><br>提现</a></div>
 </div>
</div>

<div class="sellindex1 box">
<div class="dmain flex">
 <ul class="u1"><li class="l1">账户明细</li><li class="l2"><a href="paylog.php"><img src="img/rightjian.png" /></a></li></ul>
 <ul class="u2"><li class="l1">总资产</li><li class="l2">今日收支</li></ul>
 <ul class="u3">
 <li class="l1">￥<?=returnxiaoshudian($rowuser[money1]+$rowuser[baomoney])?></li>
 <li class="l2">
 <?
 $sj1=dateYMD($sj)." 00:00:00";
 $sj2=dateYMD($sj)." 23:59:59";
 $a=returnsum("moneynum","yjcode_moneyrecord where userid=".$rowuser[id]." and moneynum<>0 and sj>='".$sj1."' and sj<='".$sj2."'");
 if($a>=0){echo "+";}
 echo returnxiaoshudian($a);
 ?>
 </li>
 </ul>
</div>
</div>

<? 
while1("*","yjcode_moneyrecord where userid=".$rowuser[id]." and moneynum<>0 order by sj desc limit 1");if($row1=mysqli_fetch_array($res1)){
if($row1[moneynum]>0){$n="收入";}else{$n="支出";}
?>
<div class="sellindex2 box">
<div class="dmain flex">
 <div class="d0"></div>
 <div class="d1" onClick="gourl('paylog.php')">最新收支：<?=$row1[sj]." ".$n." ".abs($row1[moneynum])."元"?></div>
</div>
</div>
<? }?>

<!--商品B-->
<div class="sellindex3 box">
<div class="dmain flex">
 <ul class="u1"><li class="l1">商品管理</li><li class="l2"><a href="productlist.php"><img src="img/jianright1.png" /></a></li></ul>

 <div class="swiper-containerpp">
  <div class="swiper-containerp1">
   <div class="swiper-wrapper">
   <? 
   $i=1;
   while1("*","yjcode_pro where zt<>99 and userid=".$rowuser[id]." order by lastsj desc limit 10");while($row1=mysqli_fetch_array($res1)){
   $money1=returnyhmoney($row1[yhxs],$row1[money2],$row1[money3],$sj,$row1[yhsj1],$row1[yhsj2],$row1[id]);
   $au="product.php?bh=".$row1[bh];
   ?>
   <div class="swiper-slide" onClick="gourl('<?=$au?>')">
    <div class="dv">      
     <div class="d1"><img src="<?=returntp("bh='".$row1[bh]."' order by xh asc","-1")?>" onerror="this.src='../../img/none200x200.gif'" /></div>
     <div class="d2"><?=returntitdian($row1[tit],50)?></div>
     <div class="d3">￥<?=sprintf("%.2f",$money1)?></div>
    </div>
   </div>
   <? $i++;}?>
   </div>
   <div class="swiper-paginationp1"></div>
  </div>
 </div>
 <script>
 swiperp1 = new Swiper('.swiper-containerp1', {
 slidesPerView: 3.7,
 spaceBetween:20,
 freeMode: true,
 pagination: {
 el: '.swiper-paginationp1',
 clickable: true,
 },
 });
 </script>
 
 <div class="txtadd" onClick="gourl('productlx.php')">+添加新商品</div>

</div>
</div>
<!--商品E-->

<div class="kjmenu box">
<div class="dmain flex">
 <ul class="u1"><li class="l1">快捷菜单</li><li class="l2"><a href="sellmenu.php"><img src="img/jianright1.png" /></a></li></ul>
 <ul class="u2">
 <li class="l1 la">订<br>单</li>
 <li class="l2">
 <a href="sellorder.php">商品订单</a>
 <a href="sellorder.php?ddzt=wait">等待发货</a>
 <a href="sellorder.php?ddzt=back">退款处理</a>
 </li>
 </ul>
 <? if($rowcontrol[serverhave]==1){?>
 <ul class="u2">
 <li class="l1 lb">服<br>务</li>
 <li class="l2">
 <a href="sellserverorder.php">服务订单</a>
 <a href="sellserverorder.php?ddzt=1">等待确认</a>
 <a href="sellserverorder.php?ddzt=7">服务退款</a>
 </li>
 </ul>
 <? }?>
 <ul class="u2">
 <li class="l1 lc">店<br>铺</li>
 <li class="l2">
 <a href="shop.php">基本设置</a>
 <a href="openshop4.php">店铺续费</a>
 <a href="../shop/view<?=$rowuser[id]?>.html">预览店铺</a>
 </li>
 </ul>
 <ul class="u2">
 <li class="l1 ld">互<br>动</li>
 <li class="l2">
 <a href="propjlist.php">评价管理</a>
 <a href="prowdlist.php">商品问答</a>
 <a href="../shop/view<?=$rowuser[id]?>.html">预览店铺</a>
 </li>
 </ul>
</div>
</div>

<? include("sellbottom.php");?>

<? include("../tem/globottom.php");?>

</body>
</html>