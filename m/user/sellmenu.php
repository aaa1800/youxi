<?
include("../../config/conn.php");
include("../../config/function.php");
sesCheck_m();
?>
<html>
<head>
<meta http-equiv="x-ua-compatible" content="ie=7" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
<title>会员中心 <?=webname?></title>
<? include("../tem/cssjs.html");?>
<link href="css/sell.css?t=<?=$glosxbh?>" rel="stylesheet" type="text/css" />
</head>
<body>
<? 
include("topuser.php");
$glotopfix=1;
$glotopback="sell.php";
$glotoptit="商家菜单导航";
include("../tem/glotop.php");
?>

<div class="kjmenu box">
<div class="dmain flex">
 <ul class="u2">
 <li class="l1 l0">商<br>品</li>
 <li class="l2">
 <a href="productlist.php">所有商品</a>
 <a href="productlx.php">发布商品</a>
 </li>
 </ul>
 
 <ul class="u2">
 <li class="l1 la">订<br>单</li>
 <li class="l2">
 <a href="sellorder.php">商品订单</a>
 <a href="sellorder.php?ddzt=wait">等待发货</a>
 <a href="sellorder.php?ddzt=db">待收货</a>
 <a href="sellorder.php?ddzt=back">退款处理</a>
 <a href="sellorder.php?ddzt=jf">交易纠纷</a>
 </li>
 </ul>
 <? if($rowcontrol[serverhave]==1){?>
 <ul class="u2">
 <li class="l1 lb">服<br>务</li>
 <li class="l2">
 <a href="sellserverorder.php">服务订单</a>
 <a href="sellserverorder.php?ddzt=1">等待接单</a>
 <a href="sellserverorder.php?ddzt=2">待付款</a>
 <a href="sellserverorder.php?ddzt=3">关闭订单</a>
 <a href="sellserverorder.php?ddzt=4">资金担保</a>
 <a href="sellserverorder.php?ddzt=5">买家验收</a>
 <a href="sellserverorder.php?ddzt=6">交易成功</a>
 <a href="sellserverorder.php?ddzt=7">服务退款</a>
 <a href="sellserverorder.php?ddzt=10">平台介入</a>
 </li>
 </ul>
 <? }?>
 <ul class="u2">
 <li class="l1 lc">店<br>铺</li>
 <li class="l2">
 <a href="shop.php">基本设置</a>
 <a href="openshop4.php">店铺续费</a>
 <a href="zzrz.php">执照认证</a>
 <a href="../shop/view<?=$rowuser[id]?>.html">预览店铺</a>
 </li>
 </ul>
 <ul class="u2">
 <li class="l1 ld">互<br>动</li>
 <li class="l2">
 <a href="propjlist.php">评价管理</a>
 <a href="prowdlist.php">商品问答</a>
 <a href="../shop/view<?=$rowuser[id]?>.html">预览店铺</a>
 </li>
 </ul>
</div>
</div>

<? include("../tem/globottom.php");?>

</body>
</html>