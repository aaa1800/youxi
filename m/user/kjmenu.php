<?
include("../../config/conn.php");
include("../../config/function.php");
sesCheck_m();
?>
<html>
<head>
<meta http-equiv="x-ua-compatible" content="ie=7" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
<title>会员中心 <?=webname?></title>
<? include("../tem/cssjs.html");?>
</head>
<body>
<? 
include("topuser.php");
$glotopfix=1;
$glotopback="./";
$glotoptit="快捷菜单";
include("../tem/glotop.php");
?>

<div class="kjmenu box">
<div class="dmain flex">

 <!--正常开店B-->
 <? if(2!=$rowuser[shopzt] && 4!=$rowuser[shopzt]){?>
 <? if($rowcontrol[ifopenshop]=="on"){?>
 <ul class="u2">
 <li class="l1 l0">卖<br>家</li>
 <li class="l2">
 <a href="openshop1.php">我要开店</a>
 </li>
 </ul>
 <? }?>
 
 <? }elseif(4==$rowuser[shopzt]){?>
 <ul class="u2">
 <li class="l1 l0">卖<br>家</li>
 <li class="l2">
 <a href="openshop4.php">店铺续费</a>
 </li>
 </ul>

 <? 
 }else{
 $a=returncount("yjcode_order where selluserid=".$rowuser[id]." and ddzt='wait' and admin=1");
 $b=returncount("yjcode_serverorder where selluserid=".$rowuser[id]." and ddzt=1");
 ?>
 <ul class="u2">
 <li class="l1 l0">卖<br>家</li>
 <li class="l2">
 <a href="sellorder.php">商品订单</a>
 <? if($a>0){?><a href="sellorder.php?ddzt=wait">发货<span class="red"><?=$a?></span></a><? }?>
 <a href="productlist.php">商品列表</a><a href="productlx.php">新增</a>
 <a href="sellserverorder.php">服务订单</a>
 <? if($b>0){?><a href="sellserverorder.php?ddzt=1">待确认<span class="red"><?=$b?></span></a><? }?>
 <a href="propjlist.php">评价管理</a>
 <? $a=returncount("yjcode_wenda where selluserid=".$rowuser[id]." and hftxt=''");?>
 <a href="prowdlist.php">商品问答</a>
 <a href="shop.php">店铺设置</a><a href="../shop/view<?=$rowuser[id]?>.html">预览</a>
 </li>
 </ul>
 <? }?>
 <!--卖家E-->

 <? $c=returncount("yjcode_order where userid=".$rowuser[id]." and ddzt='db' and admin=1");?>
 <ul class="u2">
 <li class="l1 ld">产<br>品</li>
 <li class="l2">
 <a href="order.php">我买到的商品</a>
 <? if($c>0){?><a href="order.php?ddzt=db">收货<span class="red"><?=$c?></span></a><? }?>
 <a href="car.php">购物车</a>
 <a href="favpro.php">我的收藏</a>
 </li>
 </ul>
 
 <ul class="u2">
 <li class="l1 lc">服<br>务</li>
 <li class="l2">
 <? $a=returncount("yjcode_serverorder where userid=".$rowuser[id]." and ddzt=5");?>
 <a href="serverorder.php">我的服务订单</a>
 <? if($a>0){?><a href="serverorder.php?ddzt=5">待验收<span class="red"><?=$a?></span></a><? }?>
 </li>
 </ul>
 
 <ul class="u2">
 <li class="l1 lb">自<br>助</li>
 <li class="l2">
  <a href="autojylist.php?ty=1">我是付款方</a>
  <a href="autojylist.php?ty=2">我是收款方</a>
  <a href="autojylx.php">发起自助</a>
 </li>
 </ul>

 <? 
 if(empty($rowcontrol[iftask])){
 $bmnum=0; //$bmnum单人任务选择用户
 while1("*","yjcode_task where taskty=0 and userid=".$rowuser[id]);while($row1=mysqli_fetch_array($res1)){
 $bmnum=$bmnum+returncount("yjcode_taskhf where bh='".$row1[bh]."' and ifxz=0 and userid=".$rowuser[id]);
 }
 ?>
 <ul class="u2">
 <li class="l1 ld">任<br>务</li>
 <li class="l2">
 <a href="tasklist.php">单人任务</a><? if($bmnum>0){?><a href="tasklist.php">选标<span class="red"><?=$bmnum?></span></a><? }?>
 <a href="tasklist1.php">多人任务</a>
 <a href="taskadd.php">发起任务</a>
 <a href="taskhflist.php">接手的单人</a>
 <a href="taskhflist1.php">接手的多人</a>
 </li>
 </ul>
 <? }?>

 <ul class="u2">
 <li class="l1 ld">互<br>动</li>
 <li class="l2">
 <a href="newslist.php">稿件管理</a>
 </li>
 </ul>
 
</div>
</div>

<? include("../tem/globottom.php");?>
</body>
</html>