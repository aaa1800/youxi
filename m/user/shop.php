<?
include("../../config/conn.php");
include("../../config/function.php");
sesCheck_m();

if($rowuser[shopzt]!=2){php_toheader("openshop3.php");}
$userid=$rowuser[id];
$usertx="../upload/".$rowuser[id]."/shop.jpg";
if(!is_file($usertx)){$usertx="img/nonetx.gif";}else{$usertx=$usertx."?id=".rnd_num(1000);}
$bannar="../upload/".$rowuser[id]."/bannar.jpg";
if(!is_file($bannar)){$bannar="img/nonetx.gif";}else{$bannar=$bannar."?id=".rnd_num(1000);}
$add1=$rowuser[areaid1];
$add2=$rowuser[areaid2];
$add3=$rowuser[areaid3];
$add1v=returnarea($add1);
$add2v=returnarea($add2);
$add3v=returnarea($add3);

if(sqlzhuru($_POST[yjcode])=="shop"){
 zwzr();
 $t1=sqlzhuru($_POST[t1]);
 $t2=sqlzhuru($_POST[t2]);
 $s1=sqlzhuru($_POST[s1]);
 if(empty($t1) || empty($t2) || empty($s1)){Audit_alert("信息不完整，返回重试！","shop.php");}
 if(panduan("*","yjcode_user where shopname='".$t1."' and uid<>'".$_SESSION[SHOPUSER]."'")==1){Audit_alert("店铺名称已经被其他用户使用，返回重试！","shop.php");}
 $area1=sqlzhuru($_POST[add1]);
 $area2=sqlzhuru($_POST[add2]);
 $area3=sqlzhuru($_POST[add3]);
 updatetable("yjcode_user","shopname='".$t1."',seokey='".$t2."',seodes='".$s1."',areaid1=".$area1.",areaid2=".$area2.",areaid3=".$area3.",gzsj='".sqlzhuru($_POST[tgzsj])."' where uid='".$_SESSION[SHOPUSER]."'");

 $myweb=trim(sqlzhuru($_POST[tmyweb]));
 if(!empty($myweb)){
  if(panduan("id,myweb","yjcode_user where myweb='".$myweb."' and id<>".$userid."")==1){Audit_alert("该自定义网址已经被使用，请更换！","shop.php");}
  if(!preg_match("/^[_a-zA-Z0-9]*$/",$myweb)){Audit_alert("自定义地址必须为数字或字母！","shop.php");}
  updatetable("yjcode_user","myweb='".$myweb."' where id=".$userid);
 }else{
  updatetable("yjcode_user","myweb='' where id=".$userid);
 }

 php_toheader("shop1.php"); 
}

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="x-ua-compatible" content="ie=7" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
<title>会员中心 <?=webname?></title>
<? include("../tem/cssjs.html");?>
<link href="css/sell.css?t=<?=$glosxbh?>" rel="stylesheet" type="text/css" />

<script language="javascript">
function tj(){
 if(document.f1.t1.value==""){layerts("请输入店铺名称");return false;}	
 if(document.f1.t2.value==""){layerts("请输入主营产品");return false;}	
 if(document.f1.s1.value==""){layerts("请输入店铺简要描述");return false;}	
 layer.open({type: 2,content: '正在提交',shadeClose:false});
 f1.action="shop.php";
}
function mywebcha(){
 document.getElementById("myweb1").innerHTML=document.f1.tmyweb.value;
}
</script>
</head>
<body>
<? 
include("topuser.php");
$glotopfix=1;
$glotopback="sell.php";
$glotoptit="店铺设置";
include("../tem/glotop.php");
?>

<? include("areatang.php");?>

<div class="clear clear10"></div>

<form name="f1" method="post" onSubmit="return tj()">
<input type="hidden" value="<?=$add1?>" id="add1" name="add1" />
<input type="hidden" value="<?=$add2?>" id="add2" name="add2" />
<input type="hidden" value="<?=$add3?>" id="add3" name="add3" />
<input type="hidden" value="shop" name="yjcode" />

<div class="shuru box">
 <div class="d1">当前到期</div>
 <div class="d21" onClick="gourl('openshop4.php')"><?=$rowuser[dqsj]?></div>
</div>

<div class="shuru box">
 <div class="d1">店铺名称</div>
 <div class="d2"><input type="text" class="inp" value="<?=$rowuser[shopname]?>" name="t1" /></div>
</div>

<div class="shuru box" onClick="qytang()">
 <div class="d1">所在地区<span class="s1"></span></div>
 <div class="d21" id="qyname"><?=$add1v.$add2v.$add3v?></div>
</div>

<div class="shuru box">
 <div class="d1">自定义后缀</div>
 <div class="d2"><input type="text" class="inp" onKeyUp="mywebcha()" value="<?=$rowuser[myweb]?>" name="tmyweb" /></div>
</div>

<div class="shuru box">
 <div class="d1">后缀访问形式</div>
 <div class="d21"><?=weburl?>vip<span id="myweb1"><?=$rowuser[myweb]?></span></div>
</div>

<div class="shuru box">
 <div class="d1">工作时间</div>
 <div class="d2"><input type="text" class="inp" value="<?=$rowuser[gzsj]?>" name="tgzsj" /></div>
</div>

<div class="shuru box">
 <div class="d1">主营产品</div>
 <div class="d2"><input type="text" class="inp" value="<?=$rowuser[seokey]?>" name="t2" /></div>
</div>

<div class="shuru box">
 <div class="d1">简要描述</div>
 <div class="d2"><textarea name="s1" placeholder="请输入店铺简要描述"><?=$rowuser[seodes]?></textarea></div>
</div>

<div class="shuru box">
 <div class="d1">店铺LOGO</div>
 <div class="d2"><iframe src="tpupload.php?admin=11" scrolling="no" frameborder="0"></iframe></div>
</div>

<div class="shuru shuru0 box">
 <div class="d1">店招横幅</div>
 <div class="d2"><iframe src="tpupload.php?admin=12" scrolling="no" frameborder="0"></iframe></div>
</div>

<? tjbtnr_m("下一步")?>
</form>

<? include("../tem/globottom.php");?>

</body>
</html>