<?
include("../../config/conn.php");
include("../../config/function.php");
sesCheck_m();

$ordering=returncount("yjcode_order where selluserid=".$rowuser[id]." and (ddzt='wait' or ddzt='db' or ddzt='back' or ddzt='backerr' or ddzt='jf')");

//入库操作开始
if($_POST[jvs]=="bao"){
 zwzr();
 if($ordering>0){Audit_alert("操作失败，因为有订单未处理","baomoney2.php");}
 $t1=floatval($_POST[t1]);
 if($t1>$rowuser[baomoney]){Audit_alert("可用保证金不足","baomoney2.php");}
 if($t1<=0){Audit_alert("未知错误","baomoney1.php");}
 PointIntoB($rowuser[id],"解冻保证金",$t1*(-1),0,1);
 PointUpdateB($rowuser[id],$t1*(-1)); 
 php_toheader("../tishi/index.php?admin=999&b=../user/baomoney2.php");
}
//入库操作结束 

?>
<html>
<head>
<meta http-equiv="x-ua-compatible" content="ie=7" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
<title>会员中心 <?=webname?></title>
<? include("../tem/cssjs.html");?>
<script language="javascript">
function tj(){
 <? if($ordering>0){?>alert("操作失败，因为有订单未处理");return false;<? }?>
 if(document.f1.t1.value==""){alert("请输入保证金数量");return false;}	
 if(!confirm("确定要解冻保证金吗？")){return false;}
 layer.open({type: 2,content: '正在提交',shadeClose:false});
 f1.action="baomoney2.php";
}
</script>
</head>
<body>
<? 
include("topuser.php");
$glotopfix=1;
$glotopback="./";
$glotoptit="解冻保证金";
include("../tem/glotop.php");
?>

<div class="clear clear10"></div>

<form name="f1" method="post" onSubmit="return tj()">
<input type="hidden" value="bao" name="jvs" />
<div class="shuru box">
 <div class="d1">可用保证金</div>
 <div class="d21"><?=sprintf("%.2f",$rowuser[baomoney])?>元</div>
</div>
<div class="shuru box">
 <div class="d1">可用余额</div>
 <div class="d21"><?=sprintf("%.2f",$rowuser[money1])?>元</div>
</div>

<? if($ordering==0){?>
<div class="shuru shuru0 box">
 <div class="d1">解冻保证金</div>
 <div class="d2"><input type="text" name="t1" class="inp" placeholder="请输入要解冻的金额" /></div>
</div>
<? tjbtnr_m("提交")?>

<? }else{?>
<div class="tishi box">
 <div class="d1">您当前还有<strong class="red"><?=$ordering?></strong>笔订单未完成处理，保证金需要等所有订单处理完毕后，才能申请解冻</div>
</div>
<? }?>

</form>

<? include("../tem/globottom.php");?>

</body>
</html>