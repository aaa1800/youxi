$(function() {
	jQuery(".banner-box .slideBox").slide({
		mainCell: ".bd ul",
		effect: "leftLoop",
		autoPlay: true,
		delayTime: 300,
		interTime: 5000
	});
	jQuery(".account-record-box .list").slide({
		mainCell: ".bd ul",
		autoPlay: true,
		effect: "leftMarquee",
		vis: 3,
		interTime: 50
	});
	jQuery(".recommend-shop-list").slide({
		titCell: ".hd ul",
		mainCell: ".bd ul",
		autoPage: true,
		effect: "leftLoop",
		autoPlay: true,
		vis: 5,
		trigger: "click"
	});
	jQuery(".hot-games-list").slide({
		mainCell: ".bd .bd-wrap-ul",
		trigger: "click"
	});
	$(".quk-area .bottom-area .search-item input").focus(function(){
		$(".quk-area .bottom-area .search-item a").css("background","#444");
	});
	$(".quk-area .bottom-area .search-item input").blur(function(){
		$(".quk-area .bottom-area .search-item a").css("background","#666");
	});
	
	$(".act-list-head .act-search input").focus(function(){
		$(this).parent().addClass("focus");
	});
	$(".act-list-head .act-search input").blur(function(){
		$(this).parent().removeClass("focus");
	});
	$(".act-head .act-search input").focus(function(){
		$(this).parent().addClass("focus");
	});
	$(".act-head .act-search input").blur(function(){
		$(this).parent().removeClass("focus");
	});
});


