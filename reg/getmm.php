<?
include("../config/conn.php");
include("../config/function.php");
?>
<html>
<head>
<meta http-equiv="x-ua-compatible" content="ie=7" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>找回密码 - <?=webname?></title>
<? include("../tem/cssjs.html");?>
<script language="javascript">
//这个短信代码不要放在JS文件中，以免跟其他页面冲突
var sz;
function yzonc(){
 t=document.f1.motmail.value;
 if(!isMot(t) && !isEmail(t)){alert("请输入有效的手机号或邮箱格式");document.f1.motmail.focus();return false;}
 if(isMot(t)){lxv="mot";}else{lxv="mail";}
 document.getElementById("sjzouv").innerHTML=120;
 document.getElementById("fs1").style.display="none";
 document.getElementById("fs2").style.display="";
 document.getElementById("yzm").focus();
 $.post("getmmchk.php",{motmail:t,lx:lxv},function(result){
	if(result=="err1"){
	 alert("请输入有效的手机号或邮箱格式");document.getElementById("motmail").focus();document.getElementById("fs1").style.display="";document.getElementById("fs2").style.display="none";return false;
	}else if(result=="err2"){
	 alert("未找到该手机/邮箱绑定的账号");document.getElementById("motmail").focus();document.getElementById("fs1").style.display="";document.getElementById("fs2").style.display="none";return false;
	}else if(result=="errmail"){
	 alert("网站未开通邮箱功能，请联系管理员");location.reload();return false;
	}else if(result=="errbig"){
	 alert("发送数量超过当日最大数量，发送失败");location.reload();return false;
	}else{
	 sz=setInterval("sjzou()",1000);return false;
	}
 });
}

function sjzou(){
 s=parseInt(document.getElementById("sjzouv").innerHTML);
 if(s<=0){
  clearInterval(sz);
  document.getElementById("sjzouv").innerHTML=120;
  document.getElementById("fs1").style.display="";
  document.getElementById("fs2").style.display="none";
  return false;
 }else{document.getElementById("sjzouv").innerHTML=s-1;}
}
</script>
</head>
<body>
<? include("../tem/top.html");?>
<? include("../tem/top1.html");?>
<div class="yjcode">

<div class="dqwz"><ul class="u1"><li class="l1">当前位置：<a href="../">首页</a> > 找回密码</li></ul></div>

<div class="getmm">

 <div class="getmmcap">
  <a href="javascript:void(0);" class="a1">01/ 请验证您的联系方式</a>
  <a href="javascript:void(0);">02/ 设定您的新密码</a>
 </div>
 
 <div class="clear clear20"></div>
 
 <form name="f1" method="post" onSubmit="return getmmtj()">
 <ul class="u1">
  <li class="l1">手机/邮箱：</li>
  <li class="l2"><input type="text" name="motmail" id="motmail" autocomplete="off" disableautocomplete placeholder="请输入绑定的手机或邮箱" /></li>
 </ul>
 <ul class="u1">
  <li class="l1">验证码：</li>
  <li class="l2">
  <input type="text" name="yzm" class="inp1" id="yzm" autocomplete="off" disableautocomplete placeholder="输入验证码" />
  <a href="javascript:void(0);" id="fs1" onClick="yzonc()">获取验证码</a>
  <a href="javascript:void(0);" id="fs2" style="display:none;"><span id="sjzouv">120</span>秒后重发</a>
  </li>
 </ul>
 <div class="dbtn"><input type="submit" value="下一步" /></div>
 <input type="hidden" value="getmm" name="yjcode" />
 </form>

 
</div>
</div>
<? include("../tem/bottom.html");?>
</body>
</html>